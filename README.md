# To-do List with login

This is a simple to-do application with a login built in node.js

Make sure to have mongodb running. Open extra terminal to run node. 

Install node_modules using command "npm install".

Run application using "node index.js"

Application is running on port 3000